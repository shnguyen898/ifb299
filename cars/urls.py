from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('locations/', views.locations, name="locations"),
    path('customer/<int:pk>/', views.customer, name='customer'),
    path('inventory/', views.inventory, name='inventory'),
    path('inventory/<int:pk>/', views.car_detail, name='car_detail'),
    path('management/', views.management, name='management'),
    path('management/register/', views.register, name='register'),
    path('orders/', views.order_list, name='order_list'),
    path('staff/<int:pk>', views.staff, name='staff'),
    path('store/<int:pk>/', views.store, name='store'),
    path('analytics/', views.analytics, name='analytics'),
    path('recommendation/', views.recommendation, name='recommendation'),

    path('inventory/add/', views.add_car, name='add_car'),
    path('toggle/<int:pk>/', views.toggle, name='toggle'),
    
    path('orders/new/', views.create_order, name="create_order"),
    path('orders/open/', views.open_order, name="open_order"),
    path('orders/<int:pk>/pickup/', views.pickup_car, name="pickup_car"),
    path('orders/<int:pk>/return/', views.return_car, name="return_car"),

    path('orders/<int:pk>/', views.car_history, name="car_history")
]