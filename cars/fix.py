from cars.models import Car, Order, Store

cars = Car.objects.all()

for car in cars:
    print(car.pk)

    orders = Order.objects.filter(car=car).order_by('-pk')
    order = orders[0]
    
    car.last_store = order.return_store
    print(car.pk)
    car.save()