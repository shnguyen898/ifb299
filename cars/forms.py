from django import forms
from django.contrib.auth.models import User
from .models import Order, Car, Customer, Car_History, Store

class RegisterForm(forms.ModelForm):
    email = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Email Address'}), label='')
    first_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'First Name'}), label='')
    last_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Last Name'}),label='')
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Username', 'autocomplete': 'off'}),label='')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'autocomplete': 'off'}), label='')
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password']

class OrderForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        store = Store.objects.get(pk=kwargs.pop("store"))
        super(OrderForm, self).__init__(*args, **kwargs)
        self.fields["customer"].queryset = Customer.objects.all().order_by('name')
        self.fields["car"].queryset = Car.objects.filter(available=True).filter(last_store=store.pk).order_by('make', 'model')

    class Meta:
        model = Order
        fields = ['customer', 'car']

class CarForm(forms.ModelForm):
    class Meta:
        model = Car
        fields = ['last_store', 'make', 'model', 'series', 'series_year', 'price_new', 'engine_size', 'fuel_system', 'tank_capacity', 'power', 'seating_capacity', 'standard_transmission', 'body_type', 'drive', 'wheelbase', 'available']


        #         last_store = form.cleaned_data.get['value']
#         make = request.POST.get('make', '')
#         model = request.POST.get('model', '')
#         series = request.POST.get('series', '')
#         series_year = request.POST.get('series_year', '0')
#         price_new = request.POST.get('price_new', '0')
#         engine_size = request.POST.get('engine_size', '0.0')
#         fuel_system = request.POST.get('fuel_system', '0.0')
#         tank_capacity = request.POST.get('tank_capacity', '0.0')
#         power = request.POST.get('power', '0')
#         seating_capacity = request.POST.get('seating_capacity', '0')
#         standard_transmission = request.POST.get('standard_transmission', '')
#         body_type = request.POST.get('body_type', '')
#         drive = request.POST.get('drive', '')
#         wheelbase = request.POST.get('wheelbase', '0')
#         # last_store = request.POST.get('last_store', pid)
#         available = request.POST.get('available', '0.0')