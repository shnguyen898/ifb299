from django.db import models
from django.utils import timezone

class Store(models.Model):
    store_name = models.CharField(max_length=200, null=False)
    address = models.CharField(max_length=200, null=False)
    phone = models.CharField(max_length=20, null=False)
    city = models.CharField(max_length=100, null=False)
    post_code = models.IntegerField(null=False)
    state = models.CharField(max_length=100, null=False)

    def __str__(self):
        return self.store_name

class Car(models.Model):
    make = models.CharField(max_length=200, null=False)
    model = models.CharField(max_length=200, null=False)
    series = models.CharField(max_length=200, null=False)
    series_year = models.IntegerField(null=False)
    price_new = models.IntegerField(null=False)
    engine_size = models.FloatField(null=False) 
    fuel_system = models.CharField(max_length=100, null=False)
    tank_capacity = models.FloatField(null=False)
    power = models.IntegerField(null=False)
    seating_capacity = models.IntegerField(null=False)
    standard_transmission = models.CharField(max_length=10, null=False)
    body_type = models.CharField(max_length=100, null=False)
    drive = models.CharField(max_length=4, null=False)
    wheelbase = models.IntegerField(null=False)
    last_store = models.ForeignKey(Store, on_delete=models.DO_NOTHING, related_name="last_store")
    available = models.BooleanField(null=False, default=True)

    def __str__(self):
        return(self.make + " " + self.model)

class Customer(models.Model):
    name = models.CharField(max_length=200, null=False)
    phone = models.CharField(max_length=20, null=False)
    addresss = models.CharField(max_length=200, null=False)
    city = models.CharField(max_length=100, null=False)
    state = models.CharField(max_length=100, null=False)
    post_code = models.IntegerField(null=False)
    brithday = models.DateField(null=False)
    occupation = models.CharField(max_length=200)
    gender = models.CharField(max_length=50)   

    def __str__(self):
        return self.name

# Model for the orders database
class Order(models.Model):
    # Date Order was created
    creation_date = models.DateField(default=timezone.now, null=False)
    
    # Date car was picked up and returned
    pickup_date = models.DateField(null=True, blank=True)
    return_date = models.DateField(null=True, blank=True)
    
    # Renting Customer
    customer = models.ForeignKey(Customer, on_delete=models.DO_NOTHING, null=False, related_name='customer')
    
    # Rented Car
    car = models.ForeignKey(Car, on_delete=models.DO_NOTHING, null=False, related_name='car')
    
    # Location of car pickup and dropoff
    pickup_store = models.ForeignKey(Store, on_delete=models.DO_NOTHING, null=False, related_name='pickup_store')
    return_store = models.ForeignKey(Store, on_delete=models.DO_NOTHING, related_name='return_store', null=True, blank=True)
    
    complete = models.BooleanField(null=False, default=False)

    # Updates the fields for recieving a car to the current date and store
    def received(self, store):
        self.return_date = timezone.now()
        self.return_store = store
        self.complete = True
        self.save()

    # Str format
    def __str__(self):
        return str(self.pk)

class Car_History(models.Model):
    FUEL_LEVEL_CHOICES = (
        ('0', '0%'),
        ('10', '10%'),
        ('20', '20%'),
        ('30', '30%'),
        ('40', '40%'),
        ('50', '50%'),
        ('60', '60%'),
        ('70', '70%'),
        ('80', '80%'),
        ('90', '90%'),
        ('100', '100%')
    )


    interior_damage = models.TextField(null=False, default='n/a')
    interior_condition = models.TextField(null=False, default='n/a')
    exterior_damage = models.TextField(null=False, default='n/a')
    exterior_condition  = models.TextField(null=False, default='n/a')
    comments = models.TextField(null=False, default='n/a')
    fuel = models.CharField(
        max_length=10,
        choices=FUEL_LEVEL_CHOICES,
        null=False
    )
    order = models.ForeignKey(Order, on_delete=models.DO_NOTHING, null=False, related_name='order')

    def __str__(self):
        return str(self.pk)
