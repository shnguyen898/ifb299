from django.contrib import admin
from .models import Car, Customer, Order, Store, Car_History

admin.site.register(Car)
admin.site.register(Customer)
admin.site.register(Order)
admin.site.register(Store)
admin.site.register(Car_History)
