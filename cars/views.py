from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.db import IntegrityError, connection
from .forms import RegisterForm, OrderForm, CarForm
from .models import Car, Store, Order, Customer, Car_History
from django.conf import settings
from django.db.models import Q, Count
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.urls import reverse
from django import forms
from datetime import datetime    

# Checks if a user is in a designated group
def in_group(request, group):
    return request.user.groups.filter(name=group).exists()

def home(request):
    return render(request, 'cars/home.html', {})

def locations(request):
    location_list = Store.objects.all()

    paginator = Paginator(location_list, 18) # Show 25 contacts per page
    
    stores = paginator.get_page(request.GET.get('page'))

    return render(request, 'cars/locations.html', {'stores' : stores})
    
def analytics(request):
    completed_count = Order.objects.filter(complete=True).count()
    customer_count = Customer.objects.count()
    car_favourite = Car.objects.values_list('make').annotate(car_count=Count('make')).order_by('-car_count')
    store_favourite = Store.objects.values_list('store_name').annotate(store_count=Count('store_name')).order_by('-store_count')
    orders = Order.objects.filter(complete=True).filter(return_date__gte = datetime.now().replace(day=1))
    revenue = 0
    for order in orders:
        price = order.car.price_new
        revenue += price

    return render(request, 'cars/analytics.html', {'count': completed_count, 'client_count': customer_count, 'car_count': car_favourite[0][0], 'store_count': store_favourite[0][0], 'revenue': revenue})


# def calculate_bookings(request):
#     completed_count = Order.objects.filter(complete=True).count()
#     return render(request, template, {'count': completed_count})

def add_car(request):
    if request.method == 'POST':
        form = CarForm(request.POST)

        if form.is_valid():
            car_obj = form.save()
            return HttpResponseRedirect('/inventory/add/')
        else:
            print(form.errors)
    else:
        form = CarForm()
    stores = Store.objects.all()
    return render(request, 'cars/inventory-add.html', {'form': form, 'stores': stores})



@login_required
def customer(request, pk):
    if not in_group(request, 'Retail'):
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))

    orders = Order.objects.filter(customer_id=pk)
    cars = Car.objects.all()
    customer = get_object_or_404(Customer, pk=pk)
    return render(request, 'cars/customer.html', {'customer': customer, 'orders':orders, 'cars': cars})


@login_required
def car_history(request, pk):
    if not in_group(request, 'Retail'): 
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))
    
    if Car_History.objects.filter(order_id=pk):
        history = Car_History.objects.get(order_id=pk)
        order = Order.objects.get(pk=pk)
        return render(request, 'cars/car_history.html', {'history': history, 'order' : order})
    order = Order.objects.get(pk=pk)
    return render(request, 'cars/car_history.html', {'order' : order})

@login_required
def management(request):
    if not in_group(request, 'HR'):
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))
    
    if request.GET.get('q', '') == '':
        user_list = User.objects.all()
    else:
        q = request.GET.get('q', '')
        query = Q(username__icontains=q)|Q(email__icontains=q)|Q(first_name__icontains=q)|Q(last_name__icontains=q)
        user_list = User.objects.filter(query)
    paginator = Paginator(user_list, 25) # Show 25 contacts per page
    
    users = paginator.get_page(request.GET.get('page'))

    return render(request, 'cars/management.html', {'users': users})

@login_required
def staff(request, pk):
    # Checks if User is in the HR permissions group
    if not in_group(request, 'HR'):
        # If not redirect to login page
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))

    # Retrives staff if cant be found redirect to 404 page
    staff = get_object_or_404(User, pk=pk)

    # Collects all available permission groups
    groups = Group.objects.all()

    # Checks for performed operations
    if request.method == 'POST':
        # Removing User
        if request.POST.get('remove', False):
            staff.delete() # Remove user
            return redirect('management') # Redirects to management page
        
        # Updating permissions
        elif request.POST.get('permission', False):
            for group in groups:
                if request.POST.get(group.name, False):
                    group.user_set.add(staff) # Add all selected groups
                else:
                    group.user_set.remove(staff) # Remove all unselected groups
            return redirect('staff', pk=staff.pk) # Redirects to same page to stop form resubmition

    # If no operations renders staff page
    return render(request, 'cars/staff.html', {'staff': staff, 'groups':groups})

@login_required
def order_list(request):
    # Checks if User is in the retail permissions group
    if not in_group(request, 'Retail'):
        # If not redirect to login page
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))

    # Code for handling search
    # Checks if there was a search
    if request.GET.get('q', '') == '':
        # No search return all 
        order_list = Order.objects.all()
    else:
        # Match search query to id, make, model, customer, stores
        q = request.GET.get('q', '')
        query = Q(id__icontains=q)|Q(car__make__icontains=q)|Q(car__model__icontains=q)| \
        Q(customer__name__icontains=q)|Q(pickup_store__store_name__icontains=q)| \
        Q(return_store__store_name__icontains=q)
        
        order_list = Order.objects.filter(query)
    
    # Converts list of orders into a paged list
    paginator = Paginator(order_list, 25) # Show 25 contacts per page
    
    # Returns order list and template for rendering
    orders = paginator.get_page(request.GET.get('page'))
    return render(request, 'cars/order_list.html', {'orders': orders})


def store(request, pk):
    store = get_object_or_404(Store, pk=pk)
    stocks = Car.objects.filter(available=True, last_store_id=pk)
    return render(request, 'cars/store.html', {'store': store, 'stocks' : stocks})


def inventory(request):
    if request.user.is_authenticated:
        car_list = Car.objects.all()
    else:
        car_list = Car.objects.filter(available=True)
    if request.GET.get('q', '') == '':
        pass
    else:
        q = request.GET.get('q', '')
        query = Q(id__icontains=q)|Q(make__icontains=q)|Q(model__icontains=q)|Q(series__icontains=q)|Q(series_year__icontains=q)|Q(price_new__icontains=q)|Q(last_store__store_name__icontains=q)
        car_list = car_list.filter(query)

    paginator = Paginator(car_list, 25) # Show 25 contacts per page
    
    cars = paginator.get_page(request.GET.get('page'))

    return render(request, 'cars/inventory.html', {'cars': cars})

def car_detail(request, pk):
    car = get_object_or_404(Car, pk=pk)
    store = get_object_or_404(Store, pk=car.last_store.pk)

    histories = Car_History.objects.filter(order_id__car_id=pk)
    
    return render(request, 'cars/car_detail.html', {'car': car, 'store': store, 'histories': histories})

@login_required
def toggle(request, pk):
    if not in_group(request, 'Manager'):
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))
    car = get_object_or_404(Car, pk=pk)
    car.available = not car.available
    car.save()
    
    return redirect('car_detail', pk=pk)

@login_required
def register(request):
    if not in_group(request, 'HR'):
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))

    if request.method == 'POST':
        data = request.POST

        form = RegisterForm(data)
        
        username = data['username']
        password = data['password']
        fname = data['first_name']
        lname = data['last_name']
        email = data['email']

        try:
            user = User.objects.create_user(username, email, password)
            user.first_name = fname
            user.last_name = lname
            user.save()
        except IntegrityError as e:
            form.error = "User is already being used"
            return render(request, 'cars/register.html', {'form': form})

        return redirect('management')
    else:
        form = RegisterForm
    return render(request, 'cars/register.html', {'form': form})

@login_required
def create_order(request):
    # Checks if User is in the retail permissions group
    if not in_group(request, 'Retail'):
        # If not redirect to login page
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))

    # Retrieves list of all stores
    stores = Store.objects.all()

    if request.method == 'POST':
        # Get selected store
        store = request.POST.get('store')

        # If store has been picked show order create form
        if (store != None):
            # Generates form with cars from store
            form = OrderForm(request.POST, store=store)
            
            # Check if returned form data is valid
            if form.is_valid():
                order = form.save(commit=False)
                
                # Update orders pick up location
                pstore = Store.objects.get(pk=store)
                order.pickup_store = pstore

                # Save order to table
                order.save()

                # Set car as unavailable
                car = Car.objects.get(pk=order.car.pk)
                car.available = False
                car.save()

                # Redirect to active orders page
                return redirect('open_order')
            else:
                form = OrderForm(store=store)
            # Return order create form
            return render(request, 'cars/create_order.html', {'store': store, 'form': form})
    # Return store select form
    return render(request, 'cars/create_order.html', {'stores': stores})

@login_required
def open_order(request):
    # Checks if User is in the retail permissions group
    if not in_group(request, 'Retail'):
        # If not redirect to login page
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))

    # Code for handling search
    # Checks if there was a search
    if request.GET.get('q', '') == '':
        # No search return all 
        order_list = Order.objects.filter(return_date__isnull=True)
    else:
        # Match search query to id, make, model, customer, stores
        q = request.GET.get('q', '')
        query = Q(id__icontains=q)|Q(car__make__icontains=q)|Q(car__model__icontains=q)| \
        Q(customer__name__icontains=q)|Q(pickup_store__store_name__icontains=q)| \
        Q(return_store__store_name__icontains=q)
        
        order_list = Order.objects.filter(query).filter(return_date__isnull=True)
    
    # Converts list of orders into a paged list
    paginator = Paginator(order_list, 25) # Show 25 contacts per page
    
    # Returns order list and template for rendering
    orders = paginator.get_page(request.GET.get('page'))
    return render(request, 'cars/active_order_list.html', {'orders': orders})

@login_required
def pickup_car(request, pk):
    # Checks if User is in the retail permissions group
    if not in_group(request, 'Retail'):
        # If not redirect to login page
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))

    # Updates orders pick_up date to today
    order = Order.objects.get(pk=pk)
    order.pickup_date = timezone.now()
    order.save()

    # Redirects user to open_orders page when complete
    return redirect('open_order')

@login_required
def return_car(request, pk):
    # Checks if User is in the retail permissions group
    if not in_group(request, 'Retail'):
        return redirect('{}?next={}'.format(settings.LOGIN_URL, request.path))

    # Trys to find the order to be updated
    # Redirects to 404 page if not found
    order = get_object_or_404(Order, pk=pk)

    # Checks if order has been picked up yet
    if order.pickup_date == None:
        return redirect('open_order')

    # Checks for return_car data coming back
    if request.method == 'POST':
        # Retrieves return store from form
        return_store = Store.objects.get(pk=request.POST.get('return_store'))

        # Updates order to complete status with return store
        order.received(return_store)

        # Update car as available and residing at return_store
        car = order.car
        car.available = True
        car.last_store = return_store
        car.save()

        # Create row for table insertion
        history = Car_History()
        history.interior_damage=request.POST.get('interior_damage', '')
        history.interior_condition=request.POST.get('interior_condition', '')
        history.exterior_damage=request.POST.get('exterior_damage', '')
        history.exterior_condition=request.POST.get('exterior_condition', '')
        history.comments = request.POST.get('comments', '')
        history.fuel = request.POST.get('fuel')
        history.order = order
        history.save()

        return redirect('order_list')

    # Get list of all stores for form
    stores = Store.objects.all()

    # Returns template and data for browser to render
    return render(request, 'cars/return_car.html', {'order': order, 'stores': stores})

def recommendation(request):
    if request.method == 'POST':
        cars = Car.objects.filter(available =True)

        if request.POST.get('store'):
            cars=cars.filter(last_store=request.POST.get('store'))

        if request.POST.get('seat'):
            cars=cars.filter(seating_capacity=request.POST.get('seat'))

        if request.POST.get('drive'):
            cars=cars.filter(drive=request.POST.get('drive'))

        if request.POST.get('body_type'):
            cars=cars.filter(body_type=request.POST.get('body_type'))
            
        paginator = Paginator(cars, 25)
        cars = paginator.get_page(request.GET.get('page'))
        
        if not cars:
           return redirect('recommendation')
        return render(request, 'cars/recommendation.html', {'cars': cars})

    else:
        stores = Store.objects.all()
        seats = Car.objects.values('seating_capacity').distinct().order_by('seating_capacity')
        drives = Car.objects.values('drive').distinct().order_by('drive')
        body_types = Car.objects.values('body_type').distinct().order_by('body_type')
        return render(request, 'cars/recommendation.html', {'stores': stores,'drives': drives,'seats': seats,'body_types': body_types})  