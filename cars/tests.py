import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import random

class RecommendationTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.close()

    def testPageTitle(self):
        self.browser.get('localhost:8000/recommendation')
        self.assertIn('Recommendations', self.browser.title)

    def testCheckAllEmptyFields(self):
        self.browser.get('localhost:8000/recommendation')
        button = self.browser.find_element_by_xpath("//button[@type='submit']")
        button.click()
        
        self.assertIn('Recommendations', self.browser.title)

    def testStoreSet(self):
        self.browser.get('localhost:8000/recommendation')
        store_input = self.browser.find_element_by_id('store')

        store_input.send_keys(Keys.DOWN)
                
        button = self.browser.find_element_by_xpath("//button[@type='submit']")
        button.click()
        self.assertIn('Alexandria', self.browser.page_source)

    def testResetPage(self):
        self.browser.get('localhost:8000/recommendation')

        button = self.browser.find_element_by_xpath("//button[@type='submit']")
        button.click()

        reset_input = self.browser.find_element_by_link_text('Reset')
        reset_input.click()
        
        self.assertIn('Select Store', self.browser.page_source)

    def testPaginationForwards(self):
        self.browser.get('localhost:8000/recommendation')

        button = self.browser.find_element_by_xpath("//button[@type='submit']")
        button.click()

        next_button = self.browser.find_element_by_link_text('next >')
        next_button.click()

        self.assertIn('?page=2',self.browser.current_url)

    def testPaginationBackwards(self):
        self.browser.get('localhost:8000/recommendation')

        button = self.browser.find_element_by_xpath("//button[@type='submit']")
        button.click()

        next_button = self.browser.find_element_by_link_text('next >')
        next_button.click()
        next_button.click()

        back_button = self.browser.find_element_by_link_text('< previous')
        back_button.click()

        self.assertIn('?page=2',self.browser.current_url)

    def testNavLinks(self):
        self.browser.get('localhost:8000/recommendation')

        home_button = self.browser.find_element_by_link_text('HOME')
        home_button.click()
        
        self.assertIn('Home', self.browser.title)

if __name__ == '__main__':
    unittest.main(verbosity=2)